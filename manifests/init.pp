
# configure apt

class certbot::repo()
    {
    $distname = $facts['os']['distro']['codename']
    
    apt::source 
        {
        "$distname-backports":
          location => "http://ftp.debian.org/debian",
          release => "$distname-backports",
          repos => "main",
        }
    }

class certbot::package()
    {
    require certbot::repo
    
    $distname = $facts['os']['distro']['codename']

    package
        {
        'certbot':
        ensure => installed,
        install_options => {'-t' => "$distname-backports"},
        require => Exec['apt_update'],
        }
    }

class certbot($email, $domains, $dhparam)
    {
    require certbot::package

    $domains.each |String $domain|
        {
        # Once a certificate is obtained, updates will be handled by cron job. We only
        # check for the existence of the directory containing certificates for $domain.
        exec
            {
            "/usr/bin/certbot certonly --authenticator standalone -m '$email' --keep-until-expiring --agree-tos --quiet -d '$domain'":
            creates => "/etc/letsencrypt/live/$domain",
            }
        }
 
    file
        {
        '/etc/letsencrypt/dhparam.pem':
        ensure => present,
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => "$dhparam",
        }
        
    file
        {
        '/etc/cron.d/certbot':
        ensure => present, 
        owner => 'root', 
        group => 'root', 
        mode => '0644',
        source => "puppet:///modules/certbot/certbot_cron",
        }
    }

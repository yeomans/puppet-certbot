# Certbot Puppet Module

A minimal representation of certbot configuration and certificate creation in Puppet.  As
written, it is for use with nginx and debian stretch.
